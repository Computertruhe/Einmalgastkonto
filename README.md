# Einmalgastkonto für öffentlich zugängliche Computer
Der hier hinterlegte Code dient der dynamischen Erstellung individuell verschlüsselter Einmalgastkonten, die für den Einsatz auf öffentlich zugänglichen Linux (Mint)-Rechnern vorgesehen sind, wie z. B. in Geflüchtetenunterkünften oder Quartiertreffs. Da sie von zahlreichen unbekannten Benutzer\*innen genutzt werden, wird der Schutz der Privatsphäre in den Vordergrund gestellt.

# Installation des Basissystems
Als Grundlage nutzen wir hier – wie auch bei unseren meisten anderen Rechnern – Linux Mint in der aktuellen Version (zum Zeitpunkt der Erstellung dieses Dokuments ist dies 20.3) und unsere zusätzliche Software-Auswahl und Konfiguration, wie sie in der Prozessbeschreibung „Instandsetzung der Rechner“ dokumentiert ist.

# Benutzer
## Hauptbenutzer erstellen
Im folgenden Beispiel wird von einem Computer ausgegangen, auf dem während der Installation nur ein Benutzer eingerichtet wurde. Dieser Benutzer hat unter Ubuntu-Derivaten traditionell alle Rechte, um den Computer zu warten, zu pflegen, Programme zu installieren, die Bildschirmauflösung einzustellen oder das Erscheinungsbild anzupassen.

Dieser Benutzer wird im weiteren Verlauf dieser Dokumentation als „Hauptbenutzer“ bezeichnet und heißt im konkreten Fall *Computertruhe e. V.* bzw. intern `computertruhe`. Er erstellt die Grundkonfiguration des Rechners und richtet weitere Benutzer ein.

Bei der Einrichtung dieses Benutzers ist darauf zu achten, dass *Mein persönlichen Ordner verschlüsseln* ausgewählt wird.

## Vorlage für Gastnutzer
### Benutzer erstellen
Benutzer `gast_vorlage` anlegen und mit einem starken Passwort versehen.
    sudo adduser --gecos "Gast (Vorlage)" gast_vorlage
Dieser Benutzer dient zukünftig als Blaupause für jeden neu erzeugten Gastbenutzer.

### Benutzer konfigurieren
Anschließend mit dem neuen Benutzer `gast_vorlage` anmelden und die gewünschten Einstellungen vornehmen. Am Beispiel der Rechner für den *Bürgertreff Kollnau* werden folgende Änderungen durchgeführt:

* *Aktualisierungsverwaltung* starten und den Willkommensdialog bestätigen
    * Unter *Bearbeiten* > *Einstellungen* > *Optionen* die folgenden Punkte aktivieren:
        * *Nach dem Anwenden der Aktualisierungen die Aktualisierungsverwaltung verbergen*
        * *Das Systemleistensymbol nur anzeigen, wenn Aktualisierungen verfügbar sind oder im Falle eines Fehlers*
* Schnellstartleiste
    * *Terminal* entfernen
    * *LibreOffice* (Starter) hinzufügen
* Austausch des Hintergrundbildes
* Lautstärke prüfen und entsprechend einstellen

Abschließend den Benutzer `gast_vorlage` abmelden.

### Benutzer als Vorlage festlegen
Hierzu wird mit dem Hauptbenutzer in der Datei `/etc/adduser.conf` wird der Wert der Variablen `SKEL` `/etc/skel` durch `/home/gast_vorlage` ersetzt. Wird nun über den Befehl `adduser`, wie im `resetGuestAccount`-Skript, ein neuer Benutzer erzeugt, wird grundsätzlich `gast_vorlage` als Basis für den neuen Benutzer verwendet.

# Aktualisierungsverwaltung konfigurieren
*Aktualisierungsverwaltung* öffnen und folgende Einstellungen vornehmen:

* Unter *Bearbeiten* > *Systemschnappschüsse* Systemschnappschüsse aktivieren und im Wizard die Standardeinstellungen übernehmen. Als Schnappschussebenen *Monatlich* und *Wöchentlich* aktivieren und die Werte *2* und *3* zuweisen.
* Unter *Bearbeiten* > *Einstellungen* > *Automatisierung* die folgenden Punkte aktivieren:
    * *Aktualisierungen automatisch anwenden*
    * *Cinnamon-Spices automatisch aktualisieren*
    * *Flatpaks automatisch aktualisieren*
    * *Veraltete Kernel und Abhängigkeiten entfernen*

# Bildschirmschoner konfigurieren
*Bildschirmschoner* öffnen und im Reiter *Einstellunge*n die folgenden Änderungen vornehmen:

* Bereich *Bildschirmschonereinstellungen*
    * *Verzögerung vor dem Start des Bildschirmschoners* auf *10 Minuten* setzen
* Bereich *Sperreinstellungen*
    * *Den Rechner sperren, wenn er in den Ruhezustand geht* deaktivieren
    * *Den Rechner nach Start des Bildschirmschoners sperren* deaktivieren

# Energieverwaltung konfigurieren
*Energieverwaltung* öffnen und im Bereich *Energieoptionen* folgende Änderungen durchführen:
* *Den Bildschirm ausschalten, wenn inaktiv für* auf *10 Minuten* setzen

# Programme installieren
Für den automatischen Anmeldeprozess werden folgende Programme benötigt und müssen daher installiert werden:

    sudo apt install haveged pwgen xdotool --yes

# Skript anlegen
Im `opt`-Verzeichnis wird durch den Hauptbenutzer ein Unterverzeichnis mit dem Namen `guestEnvironment` erstellt. Darin wird das Shell-Skript `resetGuestAccount.sh` abgelegt und mittels `chmod u+x resetGuestAccount.sh` ausführbar gemacht.

# Cron Jobs anlegen
Mit dem Hauptbenutzer `sudo crontab -e` ausführen und folgende Zeilen in die Tabelle eintragen. Dabei unbedingt darauf achten, dass am Ende eine Leerzeile vorhanden ist!

Da der Cron-Dienst nur jede Minute anspringt, sorgt das unten stehende Konstrukt dafür, dass das Script alle 10 Sekunden ausgeführt wird.

    PATH="/usr/local/bin:/usr/bin:/usr/sbin:/bin"
    @reboot /bin/rm -f /opt/guestEnvironment/guest.lock && /opt/guestEnvironment/resetGuestAccount.sh >> /var/log/resetGuestAccount.log 2>&1

# Logrotate konfigurieren
Mittels `sudo vim /etc/logrotate.d/resetGuestAccount` den folgenden Inhalt anlegen:

    /var/log/resetGuestAccount.log {
            daily
            rotate 8
            missingok
            notifempty
            compress
    }