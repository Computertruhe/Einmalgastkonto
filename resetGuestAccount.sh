#!/bin/bash

export DISPLAY=:0
export XAUTHORITY=/var/lib/lightdm/.Xauthority

GUEST_USERNAME=gast
GUEST_DISPLAY_USERNAME=Gast

PREFIX_COLOR=32 # green

LOCK_FILE=`dirname $0`/"guest.lock"

PASSWORD_LENGHT=8

SLEEP_TIME=10


### Functions

function log() {
	printf "\e[%sm>>> %s (%d) -\e[0m %s\n" "$PREFIX_COLOR" "`date +"%Y-%m-%d %H:%M:%S %Z"`" $$ "$*"
}

function isAccountResetPossible() {
	local NO_OF_LIGHTDM_PROCESSES=`ps h -u lightdm | wc -l`

	if [[ $NO_OF_LIGHTDM_PROCESSES != 0 ]]; then
		if [[ ! `users` =~ "$GUEST_USERNAME" ]]; then
			if [[ -f "$LOCK_FILE" ]]; then
				log "LightDM is running and no guest is logged in but a login process is already running. The guest account won't be resetted."
			else
				log "LightDM is running and no guest is logged in. The guest account will be resetted."
				true && return
			fi
		else
			log "LightDM is running but guest is logged in. The guest account won't be resetted."
		fi
	else
		log "LightDM isn't running because someone is logged in. The guest account won't be resetted."
	fi

	false
}

function removeOldUser() {
	log "Killing all processes of user '$GUEST_USERNAME' ..."
	killall -v -w -u $GUEST_USERNAME

	log "Unmounting '/home/$GUEST_USERNAME' ..."
	umount /home/$GUEST_USERNAME/

	log "Unlinking all keys attached to the keyring ..."
	keyctl clear @u

	log "Removing old user '$GUEST_USERNAME' and its home directory ..."
	deluser --remove-home $GUEST_USERNAME
}

function lock() {
	touch "$LOCK_FILE"
}

function unlock() {
	rm -f "$LOCK_FILE"
}

function createNewUser() {
	log "Creating new user '$GUEST_USERNAME' with encrypted home directory ..."
	adduser --encrypt-home --gecos "$GUEST_DISPLAY_USERNAME" --disabled-password $GUEST_USERNAME
}

function generatePassword() {
	log "Generating random password ..."
	local PASSWORD=`pwgen -scnB $PASSWORD_LENGTH`
	log "Setting password ..."
	echo "$GUEST_USERNAME:$PASSWORD" | sudo chpasswd
	eval $1="$PASSWORD"
}

function login() {
	log "Getting password ..."
	generatePassword GUEST_PASSWORD

	log "Performing remote login ..."
	for ((i = 0 ; i < 2 ; i++)); do
		# you have to go at lease 2 steps up to be on the first entry
		xdotool key --clearmodifiers Up
	done
	xdotool key --clearmodifiers Down
	zenity --warning --title="WICHTIGE HINWEISE" --text="<big><b>WICHTIGE HINWEISE</b></big>\n\nBitte merken oder notieren Sie sich dieses <b>persönliche Einmalpasswort</b>:\n\n<span font-family='monospace' foreground='blue' size='larger' weight='heavy'>$GUEST_PASSWORD\n\n</span>Sie benötigen es nur, wenn Sie von der <b>Sperrfunktion</b> Gebrauch machen, falls Sie bspw. kurz den Rechner verlassen möchten. Sollten Sie sich versehentlich <b>aussperren</b>, muss der Rechner <b>neu gestartet</b> werden.\n\n\<span foreground='red' weight='heavy'>ACHTUNG\!</span> Sobald Sie sich abmelden, werden Ihr persönliches Gastkonto sowie das Einmalpasswort <b>unwiderruflich gelöscht</b>\! Nutzen Sie darum ein <b>externes Speichermedium</b> (z. B. einen USB-Stick), um Ihre angelegten Dateien abzuspeichern.\n\nDrücken Sie auf die Schaltfläche <b><i>OK</i></b>, um automatisch mit Ihrem persönlichen Gastkonto angemeldet zu werden." --width=500 2>/dev/null
	xdotool type --clearmodifiers "$GUEST_PASSWORD"
	xdotool key --clearmodifiers Return
}


### Main programme
while true
do
	log "Starting guest login process in $SLEEP_TIME seconds ..."

	sleep $SLEEP_TIME

	if [[ "$(id -u)" != "0" ]]; then
		log "You need root privileges to run this script."
		exit 1
	fi

	if isAccountResetPossible; then
		lock

		if [[ "$(id $GUEST_USERNAME)" ]]; then
			log "User '$GUEST_USERNAME' exists therefore it will be removed completely."

			removeOldUser
		else
			log "User '$GUEST_USERNAME' does not exist therefore it will be created the first time."
		fi

		createNewUser
		login
		unlock
	fi

	log "Finished guest login process."
done
